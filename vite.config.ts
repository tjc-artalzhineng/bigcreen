

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src')
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "./src/assets/scss/share.scss";'
      }
    }
  },

  base: '/',
  server: {
    port: 4000,    //设置服务启动端口
    open: true,    //设置服务启动时是否自动打开浏览器
    cors: true,    //允许跨域
    //设置代理
    proxy: {
      '/api': {
        target: 'http://xxx.xxx.xxx:8000',
        changeOrigin: true,
        secure: false,
        rewrite: (path) => path.replace('/api/', '/')
      }
    }
  }

})